﻿using System;

namespace SearchTreeExample {
    class Example {
        static void Main(string[] args) {
            var phoneBook = new Node<string, string>("H", "8934893294");

            phoneBook.Add("E", "8538793289");
            phoneBook.Add("B", "2313123442");
            phoneBook.Add("A", "2313123312");
            phoneBook.Add("I", "1214923421");
            phoneBook.Add("C", "6820389343");

            Console.WriteLine(phoneBook.Find("I"));

            try {
                Console.WriteLine(phoneBook.Find("Fernando-Null"));
            }
            catch (NotImplementedException) {
                Console.WriteLine("null");
            }
        }
    }

    class Node<K, T> where K : IComparable {
        private readonly K _key;
        private readonly T _data;
        private Node<K, T> _left;
        private Node<K, T> _right;

        public Node(K key, T data) {
            _key = key;
            _data = data;
        }

        public void Add(K key, T data) {
            if (key.CompareTo(_key) <= 0) {
                if (_left == null) _left = new Node<K, T>(key, data);
                else _left.Add(key, data);
            }
            else {
                if (_right == null) _right = new Node<K, T>(key, data);
                else _right.Add(key, data);
            }
        }

        public T Find(K key) {
            var comparerResult = key.CompareTo(_key);

            if (comparerResult == 0)
                return _data;
            else if (comparerResult < 0 && _left != null)
                return _left.Find(key);
            else if (comparerResult > 0 && _right != null)
                return _right.Find(key);

            throw new NotImplementedException();
        }
    }
}
