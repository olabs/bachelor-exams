﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HashTableExample {
    public class Example {
        public static void Main(string[] args) {
            var phoneBook = new PhoneBook();
            phoneBook.Add("Harry", "39848793893");
            phoneBook.Add("Frodo", "38384328432");
            phoneBook.Add("John", "8348342873");

            Console.WriteLine(phoneBook.FindPhone("Frodo"));
        }
    }


    public class PhoneBook {
        private List<(string, string)>[] _storage;

        public PhoneBook(int storageCapacity = 42) {
            _storage = new List<(string, string)>[storageCapacity];

            for (int i = 0; i < storageCapacity; i++)
                _storage[i] = new List<(string, string)>();
        }

        /// don't check duplicate items in storage
        public void Add(string personName, string phoneNumber) {
            _storage[GetHash(personName)]
                .Add((personName, phoneNumber));
        }

        public string FindPhone(string personName) {
            return _storage[GetHash(personName)]
                .Single(savedItem => personName == savedItem.Item1)
                .Item2;
        }

        private int GetHash(string key) {
            return key.Select(symbol => Convert.ToInt32(symbol)).Sum() % _storage.Count();
        }
    }
}
